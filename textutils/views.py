#created

from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    params={'name':'Prince', 'place':'Delhi'}
    return render(request,'index.html',params)
def analyze(request):
    djtext = request.POST.get('text','default')
    removepunc = request.POST.get('removepunc','off')
    spaceremover =  request.POST.get('spaceremover','off')
    uppercase = request.POST.get('uppercase','off')
    charcount = request.POST.get('charcount','off')
    purpose = ""
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    analyzed = ""
    if removepunc=="on":
        purpose = purpose+"Remove Punctuations "
        for char in djtext:
            if char not in punctuations:
                analyzed = analyzed+char
        params={'purpose':purpose,'analyzed_text':analyzed}
        djtext=analyzed
        # return render(request,'analyze.html',params)

    if spaceremover == "on":
        purpose = purpose + "Space Remover "
        for i in djtext:
            if i==" ":
                analyzed = djtext.replace(" ", "")
        params = {'purpose': purpose, 'analyzed_text': analyzed}
        djtext=analyzed
        # return render(request, 'analyze.html', params)
    if uppercase == "on":
        purpose = purpose + "Capitalize Text "
        for char in djtext:
            analyzed = analyzed+ char.upper()
        params = {'purpose': purpose, 'analyzed_text': analyzed}
        djtext=analyzed
        # return render(request, 'analyze.html', params)
    if charcount=="on":
        purpose = purpose+"Character Count"
        count = 0
        for i in range(0,len(djtext)):
            if not djtext[i]==" ":
                count = count+1
        analyzed = count
        params = {'purpose': purpose, 'analyzed_text': analyzed}
        
        # return render(request, 'analyze.html', params)

    return render(request, 'analyze.html', params)
